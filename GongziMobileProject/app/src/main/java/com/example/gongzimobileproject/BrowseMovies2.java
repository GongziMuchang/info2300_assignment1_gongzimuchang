package com.example.gongzimobileproject;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

public class BrowseMovies2 extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.HomeItem:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    ListView MoviesListView;
    ArrayList<Movie> movies;
    String genres[];
    DatabaseHelper d;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_movies_2);

        MoviesListView = (ListView) findViewById(R.id.MoviesListView);
        movies = new ArrayList<>();
        genres = getResources().getStringArray(R.array.genres_excluding_all_genres);
        d = new DatabaseHelper(this);

        String s = getIntent().getStringExtra("s");
        Cursor data = d.getData(s);

        while(data.moveToNext())
        {
            Movie movie = new Movie(data.getInt(0), data.getString(1), data.getString(2), data.getInt(3));
            movies.add(movie);
        }

        MovieListAdapter adapter = new  MovieListAdapter(this, R.layout.adapter_view_layout, movies);
        MoviesListView.setAdapter(adapter);

        MoviesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Movie movie = movies.get(i);
                int id = movie.getId();

                //String code = d.getCode(id);

                /*
                String code = "";

                String line = "";

                InputStream is = getApplicationContext().getResources().openRawResource(R.raw.links);
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                if(is != null)
                {
                    try
                    {
                        while((line=reader.readLine()) != null)
                        {
                            String[] temps = line.split("\\|");

                            if(id == Integer.parseInt(temps[0]))
                            {
                                code = temps[1];
                                break;
                            }
                        }

                        is.close();
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                 */

                Toast.makeText(getApplicationContext(),id,Toast.LENGTH_SHORT).show();

                //Uri uri = Uri.parse("https://www.imdb.com/title/" + code + "/");
                //                //Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                //                //startActivity(intent);

                return true;
            }
        });
    }

    private int getUpdateOrDeleteMovieGenreSpinnerPosition(String genre)
    {
        for(int i = 0; i < genres.length; i ++)
        {
            if(genre.equals(genres[i]))
            {
                return i;
            }
        }

        return -1;
    }
}