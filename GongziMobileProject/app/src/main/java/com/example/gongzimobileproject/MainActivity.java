package com.example.gongzimobileproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.quit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.QuitItem:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    DatabaseHelper d;
    Button browseMoviesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        d = new DatabaseHelper(this);
        browseMoviesButton = (Button) findViewById(R.id.BrowseMoviesButton);

        browseMoviesButton.setEnabled(false);
        browseMoviesButton.setText("Loading");

        d.clear();

        String data = "";

        InputStream is = this.getResources().openRawResource(R.raw.test);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        if(is != null)
        {
            try
            {
                while((data=reader.readLine()) != null)
                {
                    String[] temps = data.split("\\^");

                    int id = Integer.parseInt(temps[0]);
                    String name = temps[1];
                    int year = Integer.parseInt(temps[2]);
                    String genre = temps[3];

                    d.insertData(id, name, genre, year);
                }

                is.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        InputStream is2 = this.getResources().openRawResource(R.raw.links);
        BufferedReader reader2 = new BufferedReader(new InputStreamReader(is2));

        if(is2 != null)
        {
            try
            {
                while((data=reader2.readLine()) != null)
                {
                    String[] temps = data.split("\\|");

                    int id = Integer.parseInt(temps[0]);
                    String code = temps[1];

                    d.insertData2(id, code);
                }

                is2.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        browseMoviesButton.setEnabled(true);
        browseMoviesButton.setText("Browse");
    }

    public void BrowseMovies(View v)
    {
        if(v.getId() == R.id.BrowseMoviesButton)
        {
            Intent i = new Intent(MainActivity.this, BrowseMovies1.class);
            startActivity(i);
        }
    }

    public void AddMovie(View v)
    {
        if(v.getId() == R.id.AddMovieButton)
        {
            Intent i = new Intent(MainActivity.this, AddMovie.class);
            startActivity(i);
        }
    }
}