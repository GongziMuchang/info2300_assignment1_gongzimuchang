package com.example.gongzimobileproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class BrowseMovies1 extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.HomeItem:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    EditText BrowseMoviesStartingYearEditText;
    EditText BrowseMoviesEndingYearEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_movies_1);

        BrowseMoviesStartingYearEditText = (EditText) findViewById(R.id.BrowseMoviesStartingYearEditText);
        BrowseMoviesEndingYearEditText = (EditText) findViewById(R.id.BrowseMoviesEndingYearEditText);
    }

    public void Done(View v)
    {
        if(v.getId() == R.id.DoneButton)
        {
            if(BrowseMoviesStartingYearEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(BrowseMovies1.this);
                builder.setMessage("you must provide the starting year");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else if(BrowseMoviesEndingYearEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(BrowseMovies1.this);
                builder.setMessage("you must provide the ending year");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else
            {
                String s = "SELECT * FROM " + DatabaseHelper.TABLE_NAME + " WHERE year BETWEEN " + Integer.parseInt(BrowseMoviesStartingYearEditText.getText().toString()) + " AND " + Integer.parseInt(BrowseMoviesEndingYearEditText.getText().toString());

                Spinner BrowseMoviesGenreSpinner = (Spinner) findViewById(R.id.BrowseMoviesGenreSpinner);
                String genre = String.valueOf(BrowseMoviesGenreSpinner.getSelectedItem());

                if (!genre.equals("All Genres"))
                {
                    s += " AND genre LIKE '%" + genre + "%'";
                }

                Spinner BrowseMoviesNameSortSpinner = (Spinner) findViewById(R.id.BrowseMoviesNameSortSpinner);
                String nameSort = String.valueOf(BrowseMoviesNameSortSpinner.getSelectedItem());

                Spinner BrowseMoviesYearSortSpinner = (Spinner) findViewById(R.id.BrowseMoviesYearSortSpinner);
                String yearSort = String.valueOf(BrowseMoviesYearSortSpinner.getSelectedItem());

                if(!(nameSort.equals("Do Not Sort Name") && yearSort.equals("Do Not Sort Year")))
                {
                    if(!nameSort.equals("Do Not Sort Name") && yearSort.equals("Do Not Sort Year"))
                    {
                        if(nameSort.equals("Sort Name Ascending"))
                        {
                            s += " ORDER BY name ASC";
                        }
                        else if(nameSort.equals("Sort Name Descending"))
                        {
                            s += " ORDER BY name DESC";
                        }
                    }
                    else if(nameSort.equals("Do Not Sort Name") && !yearSort.equals("Do Not Sort Year"))
                    {
                        if(yearSort.equals("Sort Year Ascending"))
                        {
                            s += " ORDER BY year ASC";
                        }
                        else if(yearSort.equals("Sort Year Descending"))
                        {
                            s += " ORDER BY year DESC";
                        }
                    }
                    else if(!nameSort.equals("Do Not Sort Name") && !yearSort.equals("Do Not Sort Year"))
                    {
                        if(yearSort.equals("Sort Year Ascending"))
                        {
                            s += " ORDER BY year ASC, ";
                        }
                        else if(yearSort.equals("Sort Year Descending"))
                        {
                            s += " ORDER BY year DESC, ";
                        }

                        if(nameSort.equals("Sort Name Ascending"))
                        {
                            s += "name ASC";
                        }
                        else if(nameSort.equals("Sort Name Descending"))
                        {
                            s += "name DESC";
                        }
                    }
                }

                Intent i = new Intent(BrowseMovies1.this, BrowseMovies2.class);
                i.putExtra("s", s);
                startActivity(i);
            }
        }
    }
}