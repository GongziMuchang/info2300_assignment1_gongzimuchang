package com.example.gongzimobileproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class UpdateOrDeleteMovie extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.HomeItem:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    DatabaseHelper d;
    EditText UpdateOrDeleteMovieNameEditText;
    Spinner UpdateOrDeleteMovieGenreSpinner;
    EditText UpdateOrDeleteMovieYearEditText;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_or_delete_movie);

        d = new DatabaseHelper(this);
        UpdateOrDeleteMovieNameEditText = (EditText) findViewById(R.id.UpdateOrDeleteMovieNameEditText);
        UpdateOrDeleteMovieGenreSpinner = (Spinner) findViewById(R.id.UpdateOrDeleteMovieGenreSpinner);
        UpdateOrDeleteMovieYearEditText = (EditText) findViewById(R.id.UpdateOrDeleteMovieYearEditText);

        id = getIntent().getIntExtra("id", 0);
        String name = getIntent().getStringExtra("name");
        int position = getIntent().getIntExtra("position", 0);
        int year = getIntent().getIntExtra("year", 0);

        UpdateOrDeleteMovieNameEditText.setText(name);
        UpdateOrDeleteMovieGenreSpinner.setSelection(position);
        UpdateOrDeleteMovieYearEditText.setText(String.valueOf(year));
    }

    public void UpdateMovie(View v)
    {
        if(v.getId() == R.id.UpdateMovieButton)
        {
            if(UpdateOrDeleteMovieNameEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateOrDeleteMovie.this);
                builder.setMessage("The movie's name has to be provided.");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else if(UpdateOrDeleteMovieYearEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateOrDeleteMovie.this);
                builder.setMessage("The movie's year has to be provided.");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateOrDeleteMovie.this);
                builder.setMessage("Are you sure you want to update this movie?");
                builder.setCancelable(true);

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        String name = UpdateOrDeleteMovieNameEditText.getText().toString();
                        String genre = UpdateOrDeleteMovieGenreSpinner.getSelectedItem().toString();
                        int year = Integer.parseInt(UpdateOrDeleteMovieYearEditText.getText().toString());

                        d.updateData(id, name, genre, year);

                        Intent intent = new Intent(UpdateOrDeleteMovie.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setAllCaps(false);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setAllCaps(false);
            }
        }
    }

    public void DeleteMovie(View v)
    {
        if(v.getId() == R.id.DeleteMovieButton)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateOrDeleteMovie.this);
            builder.setMessage("Are you sure you want to delete this movie?");
            builder.setCancelable(true);

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    d.deleteData(id);

                    Intent intent = new Intent(UpdateOrDeleteMovie.this, MainActivity.class);
                    startActivity(intent);
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    dialogInterface.cancel();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setAllCaps(false);
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setAllCaps(false);
        }
    }
}