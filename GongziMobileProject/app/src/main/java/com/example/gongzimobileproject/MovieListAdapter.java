package com.example.gongzimobileproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MovieListAdapter extends ArrayAdapter<Movie>
{
    private Context mContext;
    private int mResource;

    public MovieListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Movie> objects)
    {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView textView1 = (TextView) convertView.findViewById(R.id.textView1);
        TextView textView2 = (TextView) convertView.findViewById(R.id.textView2);
        TextView textView3 = (TextView) convertView.findViewById(R.id.textView3);

        textView1.setText(getItem(position).getName());
        textView2.setText(getItem(position).getGenre());
        textView3.setText(String.valueOf(getItem(position).getYear()));

        return convertView;
    }
}