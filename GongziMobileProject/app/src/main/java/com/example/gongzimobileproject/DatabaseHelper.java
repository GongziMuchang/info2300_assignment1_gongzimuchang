package com.example.gongzimobileproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME = "moviesSQL.db";
    public static final String TABLE_NAME = "movies_table";
    public static final String TABLE_NAME_2 = "links_table";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + " (id INTEGER, name TEXT, genre TEXT, year INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME_2 + " (id INTEGER, code TEXT)");
    }

    public void insertData(int id, String name, String genre, int year)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("name", name);
        contentValues.put("genre", genre);
        contentValues.put("year", year);
        db.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getData(String s)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery(s, null);
        return res;
    }

    public void updateData(int id, String name, String genre, int year)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("name", name);
        contentValues.put("genre", genre);
        contentValues.put("year", year);
        db.update(TABLE_NAME, contentValues, "id = ?", new String[] {String.valueOf(id)});
    }

    public void deleteData(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "id = ?", new String[] {String.valueOf(id)});
    }

    public void clear()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.execSQL("DELETE FROM " + TABLE_NAME_2);
    }

    public void insertData2(int id, String code)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("code", code);
        db.insert(TABLE_NAME_2, null, contentValues);
    }

    public String getCode(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_NAME_2 + " WHERE id = " + id, null);
        res.moveToFirst();
        return res.getString(1);
    }
}