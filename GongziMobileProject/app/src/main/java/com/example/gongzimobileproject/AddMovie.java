package com.example.gongzimobileproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class AddMovie extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.HomeItem:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    DatabaseHelper d;
    EditText AddMovieNameEditText;
    Spinner AddMovieGenreSpinner;
    EditText AddMovieYearEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_movie);

        d = new DatabaseHelper(this);
        AddMovieNameEditText = (EditText) findViewById(R.id.AddMovieNameEditText);
        AddMovieGenreSpinner = (Spinner) findViewById(R.id.AddMovieGenreSpinner);
        AddMovieYearEditText = (EditText) findViewById(R.id.AddMovieYearEditText);
    }

    public void AddMovie(View v)
    {
        /*
        if(v.getId() == R.id.AddMovieButton)
        {
            if(AddMovieNameEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddMovie.this);
                builder.setMessage("The movie's name has to be provided.");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else if(AddMovieYearEditText.length() == 0)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddMovie.this);
                builder.setMessage("The movie's year has to be provided.");
                builder.setCancelable(true);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddMovie.this);
                builder.setMessage("Are you sure you want to add this movie?");
                builder.setCancelable(true);

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        String name = AddMovieNameEditText.getText().toString();
                        String genre = AddMovieGenreSpinner.getSelectedItem().toString();
                        int year = Integer.parseInt(AddMovieYearEditText.getText().toString());

                        d.insertData(name, genre, year);

                        Intent intent = new Intent(AddMovie.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setAllCaps(false);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setAllCaps(false);
            }
        }

         */
    }
}