package com.example.gongzimobileproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

public class Intro extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        TextView title = findViewById(R.id.title);
        String text = "Gongzi's Movies App";
        SpannableString s = new SpannableString(text);
        ForegroundColorSpan fcsBlue = new ForegroundColorSpan(Color.BLUE);
        ForegroundColorSpan fcsGreen = new ForegroundColorSpan(Color.GREEN);
        ForegroundColorSpan fcsRed = new ForegroundColorSpan(Color.RED);
        s.setSpan(fcsBlue, 0, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(fcsGreen, 9, 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(fcsRed, 16, 19, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        title.setText(s);
    }

    public void Start(View v)
    {
        if(v.getId() == R.id.StartButton)
        {
            Intent i = new Intent(Intro.this, MainActivity.class);
            startActivity(i);
        }
    }
}