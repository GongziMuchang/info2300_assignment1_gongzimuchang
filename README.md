To use this app, download Android Studio, and use it to open this app. Once opened, press the run button to run the app. Once the emulator starts, the user can add movies, delete movies, edit movies, search added movies by genre and year, sort the results by year and name, and long click on a movie to edit or delete the movie.

My license is BSD 3-Clause License.